package vortexiot.techtest.api;

public class FormatterUtil {

    public static double calculateReading(byte[] bytes) {
        //calculate the reading
        int whole = bytes[1];
        int divisor = Integer.parseInt(Byte.valueOf(bytes[2]).toString());
        StringBuilder builder = new StringBuilder();
        builder.append(whole).append(".").append(divisor).toString();
        double readingAmount = Double.valueOf(builder.toString());
        return readingAmount;
    }

    public static String formatTimeStamp(byte[] bytes) {
        //now set up the timestamp
        StringBuilder timeStampBuilder = new StringBuilder();
        for(int i = 3; i < 8; i++) {
            timeStampBuilder.append(Byte.valueOf(bytes[i]));
            for(int j = 3; j < 7; j++) {
                timeStampBuilder.append(",");
            }
        }
        return timeStampBuilder.toString();
    }
}
