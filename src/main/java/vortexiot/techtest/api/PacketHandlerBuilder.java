package vortexiot.techtest.api;

import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoCollection;

import java.util.Collections;

import static java.util.Arrays.asList;

public class PacketHandlerBuilder {

    public static PacketHandler createHeartbeat(byte[] bytes) {
        StringBuilder builder = new StringBuilder();
        //reverse the euid ready to process
        Collections.reverse(asList(bytes));
        //process and convert to string
        for(int i = 0; i < bytes.length; i++) {
            Byte b = Byte.valueOf(bytes[i]);
            String s = b.toString();
            String rep = s.substring(0, 1);
            builder.append(rep);
        }
        //set up the packet handler and return
        PacketHandler handler = new PacketHandler().withEuid(builder.toString())
                .withName("Heartbeat");
        return handler;
    }

    public static PacketHandler createGas(byte[] bytes) {
        double readingAmount = FormatterUtil.calculateReading(bytes);
        String timeStamp = FormatterUtil.formatTimeStamp(bytes);
        //set up the packet handler and return!
        PacketHandler handler = new PacketHandler()
                .withName("Gas")
                .withReading(readingAmount)
                .withTimestamp(timeStamp);

        return handler;
    }

    public static PacketHandler createParticulate(byte[] bytes) {
        double reading = FormatterUtil.calculateReading(bytes);
        String timeStamp = FormatterUtil.formatTimeStamp(bytes);
        PacketHandler handler = new PacketHandler()
                .withName("Particulate")
                .withReading(reading)
                .withTimestamp(timeStamp);
        return null;
    }

    public static PacketHandler createTimestamp(byte[] bytes) {
        //set up the timestamp and return.
        String timeStamp = FormatterUtil.formatTimeStamp(bytes);
        PacketHandler handler = new PacketHandler()
                .withName("TimeStamp")
                .withTimestamp(timeStamp);
        return handler;
    }
}
