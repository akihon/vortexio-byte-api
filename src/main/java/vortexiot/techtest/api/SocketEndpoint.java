package vortexiot.techtest.api;

import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoCollection;
import io.micronaut.websocket.WebSocketBroadcaster;
import io.micronaut.websocket.WebSocketSession;
import io.micronaut.websocket.annotation.OnClose;
import io.micronaut.websocket.annotation.OnMessage;
import io.micronaut.websocket.annotation.OnOpen;
import io.micronaut.websocket.annotation.ServerWebSocket;
import org.bson.Document;

@ServerWebSocket("/api/socket")
public class SocketEndpoint {

    private WebSocketBroadcaster broadcaster;
    private MongoClient client;

    public SocketEndpoint(WebSocketBroadcaster broadcaster, MongoClient client) {
        this.broadcaster = broadcaster;
        this.client = client;
    }

    @OnOpen
    public void onOpen(String topic, WebSocketSession session) {
       broadcaster.broadcastAsync("Connection accepted, ready to receive data");
    }

    @OnMessage
    public void onMessage(
            byte[] bytes,
            WebSocketSession session) {
        //process decode and store according to data type
        PacketHandler handler = null;
        String firstPart = Integer.toHexString(bytes[0]);
        switch (firstPart) {
            case "0x0001":
                handler = PacketHandlerBuilder.createHeartbeat(bytes);
                break;
            case "0x0002":
                handler = PacketHandlerBuilder.createGas(bytes);
                break;
            case "0x0003":
                handler = PacketHandlerBuilder.createParticulate(bytes);
                break;
            case "0x0101":
                handler = PacketHandlerBuilder.createTimestamp(bytes);
                break;
        }
        //At this point the document has been processed now store in the DB
        Document doc = new Document();
        doc.append("name", handler.name)
                .append("euid", handler.euid)
                .append("reading", handler.reading)
                .append("timestamp", handler.timestamp);

        getCollection(null).insertOne(null); //not inserting anything yet!
        //null for now, short on time will have to leave.
    }

    @OnClose
    public void onClose(
            String topic,
            String username,
            WebSocketSession session) {
        //do nothing
    }


    private MongoCollection<?> getCollection(MongoClient client) {
        return client
                .getDatabase(null)
                .getCollection(null, null);//out of time, will have to leave it
    }
}
