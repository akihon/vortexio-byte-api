package vortexiot.techtest.api;

import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoCollection;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.reactivex.Flowable;
import io.reactivex.Single;

import java.util.List;

import static com.mongodb.client.model.Filters.eq;

@Controller("/")
public class ApiEndpoint {

    private MongoClient client;

    public ApiEndpoint(MongoClient client) {
        this.client = client;
    }

    @Get("packets/{typeName}")
    public Single<? extends List<?>> getHandlers(@PathVariable String typeName){
        //use non blocking DB calls, better performance over large data sets
        //better real time data recall
        if(typeName != null) {
            return Flowable.fromPublisher(
                    getCollection(null) //null for now
                            .find(eq("name", typeName))
            ).toList();
        } else {
            return Flowable.fromPublisher(
                    getCollection(null)//null for now
                            .find()
            ).toList();
        }
    }


    private MongoCollection<?> getCollection(MongoClient client) {
        return client
                .getDatabase(null)
                .getCollection(null, null);//out of time, will have to leave it
    }
}
