
package vortexiot.techtest.api;

import java.time.LocalDateTime;

public final class PacketHandler {
    public String _id;
    public String name;
    public String euid;
    public double reading;
    public String timestamp;

    public PacketHandler withName(String name) {
        this.name = name;
        return this;
    }

    public PacketHandler withEuid(String euid) {
        this.euid = euid;
        return this;
    }

    public PacketHandler withReading(double reading) {
        this.reading = reading;
        return this;
    }

    public PacketHandler withTimestamp(String timestamp) {
        this.timestamp = timestamp;
        return this;
    }

}
