package vortexiot.techtest.api;

import com.mongodb.reactivestreams.client.MongoClient;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.websocket.WebSocketBroadcaster;
import io.micronaut.websocket.WebSocketSession;
import org.junit.jupiter.api.Test;

public class SocketEndpointTest {

    byte[] heartbeat = new byte[]{Byte.parseByte("0x04"),
            Byte.parseByte("0x04"), Byte.parseByte("0x02"),
            Byte.parseByte("0xAA"), Byte.parseByte("0xDF")};

    private SocketEndpoint socketEndpoint;

    WebSocketBroadcaster broadcaster;
    MongoClient client;
    WebSocketSession session;


    @Test
    public void testHeartbeat() {
        socketEndpoint = new SocketEndpoint(broadcaster, client);
        socketEndpoint.onMessage(heartbeat, session);

        //assertion to verify mocks called and processing completed!
    }

    @Test
    public void testGas() {

    }

    @Test
    public void testParticulate() {

    }

    @Test
    public void testTimestamp() {

    }
}
